## DMSG Ping-Pong



1. Write a Golang program that takes two numbers from the command line. Start up two DMSG clients and send the numbers from Client 1 to Client 2. Let Client 2 return the sum of the two numers to Client 1 and print the sum. 
2. Fork this repository and make a PR to this repository with the code.
3. Add information for contact in the PR. (Email, Telegram or Wechat.)



## DMSG Documentation

You can find documentation on DMSG [here](https://github.com/SkycoinProject/dmsg). 



## Guidelines

Here are some guidelines that will give you a clearer picture of what is expected in terms of coding style and conventions.

1: Branch and PR guide:
https://github.com/skycoin/skycoin/wiki/Github-branch-and-PR-guide

2: Idiomatic Go guide:
https://github.com/skycoin/skycoin/wiki/Idiomatic-Go-%28in-Skycoin%29

3: Dependency management:
https://github.com/skycoin/skycoin/wiki/Dependency-management-in-Skycoin

4: Linter:
https://github.com/golangci/golangci-lint
